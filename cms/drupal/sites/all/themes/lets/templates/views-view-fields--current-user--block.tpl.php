<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php
// Logic to get the user porfile picture url from http://dropbucket.org/node/1239

if (variable_get('user_pictures', 0)) {
  // Load the user account by ID
  $account = user_load($row->uid);

  if (!empty($account->picture)) {
    // Load user picture if only a file ID is passed, see TODO in
    // template_preprocess_user_picture for details
    if (is_numeric($account->picture)) {
      $account->picture = file_load($account->picture);
    }
    // Get filepath from the loaded file object
    if (!empty($account->picture->uri)) {
      $filepath = $account->picture->uri;
    }
  } elseif (variable_get('user_picture_default', '')) {
    // Use default user profile picture if the user hasn't provided one
    $filepath = variable_get('user_picture_default', '');
  }
  if (isset($filepath)) {
    $profile_url = file_create_url($filepath);
  }
}

?>


<div class="user">
    <?php if (isset($profile_url)) { ?>
           <div class="user-thumbnail" style="background-image: url(<?php print $profile_url; ?>);"></div>
    <?php } ?>

   <a href="<?php echo url('user/'. $row->nid); ?>">
       <span class="user-name"><?php echo $row->users_name; ?></span>
    </a>
   <!--<div class="user-statistics">
       <ul>
           <li>13 zoekertjes</li>
           <li>5 bezewen diensten</li>
           <li>46 credits</li>
           <li>12 reacties</li>
       </ul>
   </div>
   <div class="user-rating">
       <i class="fa fa-star" aria-hidden="true" style="color: #FBB03B;"></i>
       <i class="fa fa-star" aria-hidden="true" style="color: #FBB03B;"></i>
       <i class="fa fa-star" aria-hidden="true" style="color: #FBB03B;"></i>
       <i class="fa fa-star" aria-hidden="true" style="color: #FBB03B;"></i>
       <i class="fa fa-star" aria-hidden="true"></i>
   </div>-->
</div>

<?php //var_dump($row); ?>
