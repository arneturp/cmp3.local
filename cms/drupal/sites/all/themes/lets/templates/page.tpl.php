<!-- Begin page.tpl.php -->

<?php
if (!user_is_logged_in()) {
?>
    <div class="login-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <img src="<?php print $logo; ?>" alt="LETS" class="login-logo">
                    <?php print render($page['login']); ?>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>
<header>
        <nav class="top-nav clearfix">
            <div class="container">

                <div class="logo">
                    <img src="<?php print $logo; ?>" alt="LETS">
                </div>

                <?php print render($page['primary_navigation']); ?>
            </div>
        </nav>
    </header>

    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm hidden-xs">

                        <?php print render($page['primary_sidebar']); ?>
                        <!--<div class="user-thumbnail" style="background-image: url(img/avatar.jpg);"></div>
                        <span class="user-name">Eline Vargas</span>
                        <div class="user-statistics">
                            <ul>
                                <li>13 zoekertjes</li>
                                <li>5 bezewen diensten</li>
                                <li>46 credits</li>
                                <li>12 reacties</li>
                            </ul>
                        </div>-->

                </div>
                <div class="col-md-8">
                    <div class="main-content">
                        <?php print render($page['content']); ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer>
        <div class="prefooter">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <?php print render($page['footer_block_1']); ?>
                    </div>
                    <div class="col-sm-4">
                        <?php print render($page['footer_block_2']); ?>
                    </div>
                    <div class="col-sm-4">
                      <?php print render($page['footer_block_3']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <?php print render($page['footer']); ?>
            </div>
        </div>
    </footer>
<!-- End page.tpl.php -->
