<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>


<div class="zoekertje">
    <a href="<?php echo url('node/'. $row->nid); ?>">
        <h2><?php echo $row->node_title; ?></h2>
    </a>
    <p><?php echo substr($row->field_body[0]['raw']['value'], 0, 300); ?>...</p>
    <div class="meta-data">
        <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $row->field_field_groep[0]['raw']['taxonomy_term']->name; ?></span>
        <span><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo date('d-m-Y', $row->node_created);?></span>
        <span><i class="fa fa-money" aria-hidden="true"></i> <?php echo $row->field_field_kostprijs[0]['raw']['value']; ?></span>
    </div>
</div>
