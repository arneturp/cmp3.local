<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
    <meta charset="UTF-8">
    <title><?php print $head_title; ?></title>

    <!-- Meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Arne Turpyn">
    <meta name="description" content="LETS. Het online platform voor de geïnteresseerden van het Local Exchange Trading System.">
    <meta name="keywords" content="lets, vlaanderen, ruilhandel, zoekertjes, aanbod, online, platform, sociaal">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php print $styles; ?>
    <?php print $scripts; ?>

    <!-- Favicon -->
    <link rel="shortcut icon" href="icon.png">
</head>
<body>
    <?php print $page; ?>

</body>
</html>
