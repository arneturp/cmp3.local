Crossmedia publishing
======================

**Arne Turpyn**  
Crossmedia publishing  
Arteveldehogeschool  
Academiejaar 2016 - 2017  

Deployment gegevens
-------------------

Export van de database is te vinden in de folder `cms/drupal/_sql`

| Attribuut | Waarde |
|-----------|--------|
| DBname | cmp3 |
| DBuser | root |
| DBpass | rootroot |
| DBhost | localhost |


Login gegevens
--------------

| Attribuut | Waarde |
|-----------|--------|
| Username  | admin |
| Password  | artevelde |
| Role      | Administrator |

| Attribuut | Waarde |
|-----------|--------|
| Username  | fredroeg |
| Password  | artevelde |
| Role      | Administrator |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Rita |
| E-mail    | rita@example.com |
| Password  | artevelde |
| Role      | Lid |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Jonas |
| E-mail    | jonas@example.com |
| Password  | artevelde |
| Role      | Lid |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Wilfried |
| E-mail    | wilfried@example.com |
| Password  | artevelde |
| Role      | Lid |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Merel |
| E-mail    | merel@example.com |
| Password  | artevelde |
| Role      | Lid |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Hugo |
| E-mail    | hugo@example.com |
| Password  | artevelde |
| Role      | Lid |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Carla |
| E-mail    | carla@example.com |
| Password  | artevelde |
| Role      | Lid |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Peter |
| E-mail    | peter@example.com |
| Password  | artevelde |
| Role      | Groepscoördinator |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Mark |
| E-mail    | mark@example.com |
| Password  | artevelde |
| Role      | Regiocoördinator |

| Attribuut | Waarde |
|-----------|--------|
| Username  | Sabine |
| E-mail    | sabine@example.com |
| Password  | artevelde |
| Role      | Landcoördinator |
