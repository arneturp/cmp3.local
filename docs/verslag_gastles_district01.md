Gastles District01
==================

1. Over District01
---------------

District01 is onderdeel van een grotere groep van 200 bedrijven of 3500 medewerkers (in 2015), waaronder Service Design, Little Miss Robot, Boulevard...  

District01 zelf bestaat nu reeds 7 jaar, en bevat ongeveer 70 werknemers. Zij houden zich dagdagelijks bezig met **digitale producties**. Hun projecten steunen op 4 pijlers:
* .NET
* PHP
* New Media
* Chapter Two (project management, begeleidende rol)

Doorheen hun zevenjarig bestaan hebben ze al een indrukwekkend **klantenbestand** opgebouwd. Zo hebben ze al projecten mogen verzorgen voor Bebat (Drupal), Jules Destrooper (Drupal), Ici Paris XL, VDAB, De Persgroep, C&A, etc.

2. Aanpak van projecten
--------------------

De uitleg van Mante ging voornamelijk over de aanpak en het verloop van projecten. Algemeen kan deze structuur worden geschetst:

### 2.1. Idee
Bij aanvang van een project moet er natuurlijk eerst een idee zijn. Dit kan iets nieuw zijn, een probleem, of een uitbreiding op iets dat al bestaand.

### 2.2. Intake
Samen met de klant (of het sales-team), lead developer en projectmanager wordt er een eerste vergadersessie gehouden. Hierin worden er vragen gesteld over de verwachtingen van de klant. Daarnaast wordt er al even gepraat over de resources die District01 ter beschikking zal hebben.  

> Wanneer wilt de klant zijn project opgeleverd zien?  
> Hoe lang mag er aan gewerkt worden?  
> Wat zijn de resources?  

### 2.3. Inschatting en offertes
Na de eerste briefing wordt er intern een inschatting gemaakt van het werk. Bij voorkeur wordt dit besproken door een representatief team. Dit wil zeggen dat er bij voorkeur minstens 1 iemand per technologie aanwezig is op deze vergadering. Vaak bestaat deze selecte groep uit een projectmanager, SCRUM master, designer en een lead developer.  

Op basis van deze inschatting kan er een offerte gemaakt worden die een richtprijs kan terugkoppelen aan de klant.

### 2.4. Kick-off
Eens de offerte is goedgekeurd stelt de projectmanager het verloop van het project voor. De kick-off wordt bijgewoond door de klant en het team van developers. Tijdens zo'n kick-off wil District01 aan de klant tonen wat de omvang van zo'n webproject is.

### 2.5. Analyse
Alles begint nu concreet te worden, en er wordt een grondige analyse van het project gemaakt. Er wordt ingegaan op de functionele en technische specificaties van het project.

> Hoe implementeren we de caching?   
> Hoe zijn de sprints geplant?  
> Hoe wordt de planning opgevolgd?  

### 2.6. Concept
Er wordt een concept teruggekoppeld aan de klant. Tijdens deze fase ligt de nadruk op het binnenkrijgen van feeback. Het idee wordt in grote lijnen geschetst.

### 2.7. Wireframes
Van elke pagina worden wireframes ontworpen. Zo krijgt het volledige team (en de klant) een indruk van de structuur van de website / app. Deze wireframes komen tot stand door een samenwerking van front-end developers en designers.

### 2.8. Design
In Sketch, Illustrator of Photoshop wordt een design uitgewerkt. Er wordt vaak gebruik gemaakt van een style guide. Een website komt zelden alleen, en wordt dus vaak vervoegd door visitekaartjes, banners, etc. Een style guide (bij District01 meestal online) kan hierbij een grote rol spelen om uniformiteit te bekomen.

### 2.9. Ontwikkelen
Het team van developers begint aan de uitwerking van het project. Ook de planning wordt goed opgevolgd.

### 2.10. Ondersteuning
Doorheen de ontwikkeling wordt er contact gehouden met de klant. Op het einde van elke sprint test het QA (Quality Assurance) team de verschillende modules uit, op zoek naar bugs, beveiligingslekken, errors en crashes.

### 2.11. Oplevering
De klant krijgt het resultaat van het project.

### 2.12. Support en maintance
Het werk stopt niet bij oplevering. District01 voorziet ook updates, support, etc.  
Bij ieder project wordt er aan de klant ook een gebruiksaanwijzing gegeven. Daaarin staat hoe hij bvb. zelf met het ingebouwde CMS aan de slag kan.

3. Drush
-----

### 3.1. Wat is het?
Drush staat voor **Drupal Shell**, en kan gezien worden als een Zwitsers zakmes voor Drupal developers.

### 3.2. Voordelen
* Eenvoudig commando's uitvoeren
* Snel
    * Drush werkt rechtstreeks met PHP. Er is dus geen tussenkomst nodig van een webbrowser.
    * Je hoeft niet via de admin te werken
* Scriptable
    * Na installatie als executable beschikbaar op systeem
    * Apparte php.ini via CLI
        * Verschillende config dan de browser
* Geen timeout
    * php.ini timeout = 0 via CLI, andere memory limit

### 3.3. Installatie
* Verschillende niveau's
    * Globaal
    * Gebruiker
    * Scriptable
    * Drush zal automatisch de volgorde detecteren
* De installatie is het gemakklijkst via Composer
    * Iedere PHP developer komt toch vroeg of laat in aanmerking met Composer

### 3.4. Voorbeelden van commando's

| Commando | Betekenis |
|----------|-----------|
| `drush en` | (Download en) enable module |
| `drush cache-clear all` | Clear cache, d7 |
| `drush updb` | Update database |
| `drush fra, fua, fu` | Features revert, update |
| `dush vget, vset` | Get variable, set variable (bvb. `site_name`) |
| `drush ard, arr` | Dump van uw site, en dump hertellen |
| `drush ssh` |  Inloggen via ssh om op andere omgeving te werken |
| `drush si` | Installeren van drupal site volgens installatieprofiel (bvb. drupal met aantal features) |
| `drush qd` | Snelle drupal installatie, voor bvb. het testen van een module |
| `drush rr` | Registry rebuild (voor een corrupt register) |
| `drush upwd --password="123"` | Wachtwoord updaten |
| `drush uli` | Genereer login-link |

Meer commando's zijn te vinden op [Drush Commands](http://drushcommands.com/)  

### 3.5. Uitbreiding
Drush is uitbreidbaar door een module/module.drush.inc aan te maken, en te linken met een hook (hook_drush_command()).

### 3.6. Aliassen
Alle omgevingen kunnen in Drush een alias krijgen (zowel lokale omgevingen als live). Zo kunnen er gemakkelijk commado's uitgevoerd worden tussen verschillende omgevingen.

### 3.7. Sites bijwerken
Indien er een site geupdatet moet worden, kan dit met drush door middel van een paar commado's (bvb. `drush vset maintenance mode 0`).

4. Besluit
-------

### 4.1. Aanpak
Tijdens de uitleg over de aanpak van een project viel me op dat er werd benadrukt dat planning, feedback, analyse, etc. minstens even belangrijk is als de ontwikkeling zelf. Een website is vaak maar het topje van de ijsberg van zo'n project. Ook de analysten, projectmanagers, SCRUM-masters,... spelen een belangrijke rol.

### 4.2. Drush
Floris heeft me zeker overtuigd. Al vanaf de eerste seconde van zijn uitleg vond ik Drush de moeite waard om eens uit te proberen. Ik ben persoonlijk fan van CLI-tools. En Drush laat je toe om efficiënter te werken. Ik zal waarschijnlijk nog niet mijn volledige workflow omgooien, maar doorheen de eindopdracht wil ik wel evolueren met mijn gebruik van Drush.
